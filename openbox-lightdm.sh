#!/bin/bash
#https://bit.ly/2xuvndw
#Getting Openbox GUI with LightDM Display Manager
apt -y update;
apt -y upgrade;
apt -y install openbox;
apt -y install obconf;
apt -y install obmenu;
apt -y install lightdm;
reboot now;
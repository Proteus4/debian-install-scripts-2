#!/bin/bash
#https://bit.ly/2NY7mF7
apt -y update;
apt -y upgrade;

#Essential software
apt -y install sakura;
apt -y install pcmanfm;
apt -y install tint2;
apt -y install curl;
apt -y install apt-transport-https;
apt -y install software-properties-common;
apt -y install xbacklight;
apt -y install pnmixer;
apt -y install wicd;
apt -y install gsimplecal;
apt -y install tlp;
apt -y install tlp-rdw;
apt -y install tp-smapi-dkms;
apt -y install acpi-call-dkms;

#Extra Software
apt -y install geany;
apt -y install nitrogen;
apt -y install screenfetch;
apt -y install lxappearance;
apt -y install compton;
apt -y install conky;
apt -y install tightvncserver;
apt -y install lxtask;
apt -y install firefox-esr;
apt -y install putty;
apt -y install xtightvncviewer;
apt -y install pavucontrol;
apt -y install hexchat;
apt -y install gmusicbrowser;
apt -y install vlc;
apt -y install gnome-disk-utility;

#Configurations
#Create initial files / folders
mkdir /home/cameron/.config;
chown cameron:cameron /home/cameron/.config;
mkdir /home/cameron/.config/openbox;
chown cameron:cameron /home/cameron/.config/openbox;
mkdir /home/cameron/Downloads;
chown cameron:cameron /home/cameron/Downloads;
mkdir /home/cameron/.config/tint2;
chown cameron:cameron /home/cameron/.config/tint2;
mkdir /home/cameron/.config/compton;
chown cameron:cameron /home/cameron/.config/compton;
touch /home/cameron/.config/openbox/autostart.sh;
chown cameron:cameron /home/cameron/.config/openbox/autostart.sh;

#Installing Discord
cd /home/cameron/Downloads;
wget -O discord.deb "https://discordapp.com/api/download?platform=linux&format=deb";
dpkg -i /home/cameron/Downloads/discord.deb;

#Set screenfetch on boot
echo "if [ -f /usr/bin/screenfetch ]; then screenfetch; fi" >> /home/cameron/.bashrc;

#Installing compton config file
cd /home/cameron/Downloads;
wget https://gitlab.com/dwt1/dotfiles/raw/master/.config/compton/compton.conf;
chown cameron:cameron compton.conf;
mv compton.conf /home/cameron/.config/compton/;

#Installing openbox menu file
cd /home/cameron/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts-2/raw/master/cameron-menu.xml;
mv cameron-menu.xml menu.xml;
chown cameron:cameron menu.xml;
mv menu.xml /home/cameron/.config/openbox/;

#Insatlling openbox keybinds
cd /home/cameron/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts-2/raw/master/rc.xml;
chown cameron:cameron rc.xml;
mv rc.xml /home/cameron/.config/openbox/;

#Installing tint2 config file
cd /home/cameron/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts-2/raw/master/tint2rc;
chown cameron:cameron tint2rc;
mv tint2rc /home/cameron/.config/tint2/;

#Installing conky config file
cd /home/cameron/Downloads;
wget https://gitlab.com/Proteus4/debian-install-scripts-2/raw/master/.conkyrc;
chown cameron:cameron .conkyrc;
mv .conkyrc /home/cameron/;

#Installing Virtualbox
echo "deb http://download.virtualbox.org/virtualbox/debian stretch contrib" >> /etc/apt/sources.list;
wget -q -O- https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add;

#Set up autostart.sh
echo "tint2 &" >> /home/cameron/.config/openbox/autostart.sh;
echo "pnmixer &" >> /home/cameron/.config/openbox/autostart.sh;
echo "nitrogen --restore &" >> /home/cameron/.config/openbox/autostart.sh;
echo "compton --config /home/cameron/.config/compton/compton.conf &" >> /home/cameron/.config/openbox/autostart.sh;
echo "conky &" >> /home/cameron/.config/openbox/autostart.sh;

#Installing new repo software
apt -y update;
apt -y install -f
apt -y install vlc;
apt -y libdvdcss2;
apt -y install virtualbox-5.2;

#Cleaning up
rm /home/cameron/Downloads/discord.deb;
rm /root/2NY7mF7;
rm /root/2xuvndw;

echo "Please reboot now";